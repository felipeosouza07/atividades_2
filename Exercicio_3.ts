namespace Exercicio_3{
    let n1, n2, n3, nx: number;
    n1 = 3;
    n2 = 6; 
    n3 = 9;
    nx = 7;

    if(nx > n3){
        console.log(`Números em ordem decrescente: ${nx},${n3},${n2},${n1}`);
    }
    else if(nx > n2){
        console.log(`Números em ordem decrescente: ${n3},${nx},${n2},${n1}`);
    }
    else if(nx > n1){
        console.log(`Números em ordem decrescente: ${n3},${n2},${nx},${n1}`);
    }
    else{
        console.log(`Números em ordem decrescente: ${n3},${n2},${n1},${nx}`);
    }

}