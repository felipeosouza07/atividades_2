namespace exemplos_condicoes{
    let idade: number = 10;

    if(idade >= 18){
        console.log("Pode dirigir");
    }
    else{
        console.log("Não pode dirigir");
    }

    idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode dirigir");
}