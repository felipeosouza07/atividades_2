namespace Exercicio_2_{
    let nota1, nota2, nota3, peso1, peso2, peso3: number;
    nota1 = 1;
    nota2 = 7;
    nota3 = 2;
    peso1 = 2;
    peso2 = 3;
    peso3 = 5;

    let resultado: number;
    resultado = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

    console.log(`A media é ${resultado}`);

    if(resultado >= 8 && resultado <= 10){
        console.log("Nota A");
    }
    else if (resultado >= 7 && resultado < 8){
        console.log("Nota B");
    }
    else if (resultado >= 6 && resultado < 7){
        console.log("Nota C");
    }
    else if(resultado >= 5 && resultado < 6){
        console.log("Nota D");
    }
    else{
        console.log("Nota E");
    }

    /*resultado >= 8 && resultado <= 10 ? console.log("Nota A"): false;
    resultado >= 7 && resultado < 8 ? console.log("Nota B"): false;
    resultado >= 6 && resultado < 7 ? console.log("Nota C"): false;
    resultado >= 5 && resultado < 6 ? console.log("Nota D"): false;
    resultado >= 0 && resultado < 5 ? console.log("Nota E"): false; */

}